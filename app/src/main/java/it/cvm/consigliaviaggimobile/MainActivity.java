package it.cvm.consigliaviaggimobile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;
import com.example.consigliaviaggimobile.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import it.cvm.consigliaviaggimobile.activities.Login.LoginActivity;
import it.cvm.consigliaviaggimobile.activities.MapsActivity;
import it.cvm.consigliaviaggimobile.activities.recycleview.StructuresAdapter;
import it.cvm.consigliaviaggimobile.activities.searchview.SearchActivity;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionStructures;
import it.cvm.consigliaviaggimobile.dynamodb.utils.FilterStructuresClass;
import it.cvm.consigliaviaggimobile.dynamodb.utils.StructureType;
import it.cvm.consigliaviaggimobile.models.SearchBeanStructures;

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();
    private final String TABLE_NAME = "Strutture";
    private boolean flagLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button loginButton = findViewById(R.id.loginButton);
        final ImageView mapsButton = findViewById(R.id.mapsButton);

        AWSMobileClient.getInstance().initialize(getApplicationContext(), new Callback<UserStateDetails>() {
            @Override
            public void onResult(final UserStateDetails result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("UserDetails","Stato User: "+ result.getUserState().toString());
                        switch (result.getUserState()){
                            case SIGNED_IN :
                                Log.i("UserDetails","SIGNED_IN "+ AWSMobileClient.getInstance().getUsername());
                                loginButton.setText("Logout");
                                flagLogin = true;
                                break;
                            case SIGNED_OUT:
                                Log.i("UserDetails","SIGNED_OUT");
                                loginButton.setText("Login");
                                flagLogin = false;
                                break;
                            default: AWSMobileClient.getInstance().signOut();
                                Log.i("UserDetails","Default Status");
                                break;
                        }
                    }
                });

            }

            @Override
            public void onError(Exception e) {
                Log.w(TAG,"onError Main Activity");
            }
        });

        new Connect().execute();


        //Passa al Login activity
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!flagLogin){
                    Intent loginActivity = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginActivity);
                    finish();
                }else {
                    AWSMobileClient.getInstance().signOut();
                    loginButton.setText("Login");
                    flagLogin=false;
                }

            }
        });


        //Passa alla mappa interattiva
        mapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapsActivity = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(mapsActivity);
                finish();
            }
        });

    }

    public class Connect extends AsyncTask<Void, Void, Void>{
        ConnectionStructures dbStructures;

        final TextView searchView = findViewById(R.id.searchView);
        final TabLayout tabLayout = findViewById(R.id.tabTypeStructures);

        List<SearchBeanStructures> suggestionSearchStructures = new ArrayList<>();
        List<Document> listaStrutture;
        List<Document> filteredList;

        @Override
        protected Void doInBackground(Void... voids) {
             dbStructures = new ConnectionStructures(getApplicationContext(),TABLE_NAME);
             //Operazioni di estrazione di tutte le strutture

            listaStrutture = dbStructures.getALLStrutture();

            if(listaStrutture.isEmpty()){
                Log.w(TAG,"Non vi sono Strutture presenti nel DB");
                Toast.makeText(MainActivity.this,"Non vi sono strutture presenti sul db",Toast.LENGTH_SHORT).show();
            }

                //Popola la lista dei nomi delle strutture e degli object recuperate con i 3 attributi che la identificano
                for (Document nome : listaStrutture) {
                    suggestionSearchStructures.add(new SearchBeanStructures(
                            nome.get("Nome").asString(),
                            Double.parseDouble(nome.get("Latitudine").asString()),
                            Double.parseDouble(nome.get("Longitudine").asString()),
                            nome.get("Localita").asString(),
                            nome.get("Stelle").asInt(),
                            nome.get("Prezzo").asFloat()));
                }

            Log.i(TAG, "Sono presenti " + listaStrutture.size() + " strutture nel DB");

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            searchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
                    startActivity(searchIntent);
                    finish();
                }
            });

            //Se inizialmente filteredList == null allora carica alberghi (Prima tab selezionata)
                if(filteredList == null ) {
                    RecyclerView recyclerView = findViewById(R.id.listStructures);
                    RecyclerView.Adapter mAdapter;
                    RecyclerView.LayoutManager layoutManager;

                    // in content do not change the layout size of the RecyclerView
                    recyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    layoutManager = new LinearLayoutManager(MainActivity.this);
                    recyclerView.setLayoutManager(layoutManager);

                    filteredList = FilterStructuresClass.getAlberghi(listaStrutture);

                    //Errore lista filtrata non presente, passa una lista vuota
                    if (filteredList == null) {
                        Log.e(TAG, "Errore: lista filtrata non presente");
                        filteredList = new ArrayList<>();
                    }

                    mAdapter = new StructuresAdapter(filteredList);
                    recyclerView.setAdapter(mAdapter);
                }

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    RecyclerView recyclerView = findViewById(R.id.listStructures);
                    RecyclerView.Adapter mAdapter;
                    RecyclerView.LayoutManager layoutManager;

                    // in content do not change the layout size of the RecyclerView
                    recyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    layoutManager = new LinearLayoutManager(MainActivity.this);
                    recyclerView.setLayoutManager(layoutManager);

                    //filtra lista in base al tipo di tab selezionato
                    filteredList = Objects.requireNonNull(tab.getText()).toString().equals(StructureType.Alberghi.getType()) ?
                            FilterStructuresClass.getAlberghi(listaStrutture) :
                            tab.getText().toString().equals(StructureType.Ristoranti.getType())?
                                    FilterStructuresClass.getRistoranti(listaStrutture):
                                    tab.getText().toString().equals(StructureType.Attrazioni.getType())?
                                            FilterStructuresClass.getAttrazioni(listaStrutture): null;

                    //Errore lista filtrata non presente, passa una lista vuota
                    if(filteredList == null){
                        Log.e(TAG,"Errore: lista filtrata non presente");
                        filteredList = new ArrayList<>();
                    }

                    mAdapter = new StructuresAdapter(filteredList);
                    recyclerView.setAdapter(mAdapter);

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }

    }

}
