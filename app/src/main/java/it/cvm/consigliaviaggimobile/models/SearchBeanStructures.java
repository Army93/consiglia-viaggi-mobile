package it.cvm.consigliaviaggimobile.models;

public class SearchBeanStructures {
    private String nome;
    private double latitudine;
    private double longitudine;
    private int valutazione;
    private float prezzo;
    private String localita;

    public SearchBeanStructures(String nome, double latitudine, double longitudine, String localita, int valutazione, float prezzo) {
        this.nome = nome;
        this.latitudine = latitudine;
        this.longitudine = longitudine;
        this.localita = localita;
        this.prezzo = prezzo;
        this.valutazione = valutazione;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    public double getLongitudine() {
        return longitudine;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    public String getLocalita() {
        return localita;
    }

    public void setLocalita(String localita) {
        this.localita = localita;
    }

    public int getValutazione() {
        return valutazione;
    }

    public void setValutazione(int valutazione) {
        this.valutazione = valutazione;
    }

    public float getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(float prezzo) {
        this.prezzo = prezzo;
    }
}
