package it.cvm.consigliaviaggimobile.models;

import java.util.List;


public class Struttura {
    private double latitudine;
    private double longitudine;
    private String nome;
    private String indirizzo;
    private int stelle;
    private float prezzoMedio;
    private String descrizione;
    private String orario;
    private String tipoStruttura;
    private String imageContent;

    private String localita;
    List<Recensione> recensioni;

    public Struttura(double latitudine, double longitudine, String nome, String indirizzo, int stelle, float prezzoMedio, String descrizione, String orario , String tipo, String localita) {
        this.latitudine = latitudine;
        this.longitudine = longitudine;
        this.nome = nome;
        this.indirizzo = indirizzo;
        this.stelle = stelle;
        this.prezzoMedio = prezzoMedio;
        this.descrizione = descrizione;
        this.orario = orario;
        this.tipoStruttura = tipo;
        this.localita = localita;
    }

    public Struttura(double latitudine, double longitudine, String nome, String indirizzo) {
        this.latitudine = latitudine;
        this.longitudine = longitudine;
        this.nome = nome;
        this.indirizzo = indirizzo;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    public double getLongitudine() {
        return longitudine;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public int getStelle() {
        return stelle;
    }

    public void setStelle(int stelle) {
        this.stelle = stelle;
    }

    public float getPrezzoMedio() {
        return prezzoMedio;
    }

    public void setPrezzoMedio(float prezzoMedio) {
        this.prezzoMedio = prezzoMedio;
    }

    public List<Recensione> getRecensioni() {
        return recensioni;
    }

    public void setRecensioni(List<Recensione> recensioni) {
        this.recensioni = recensioni;
    }

    public String getDescrizione() { return descrizione; }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getOrario() { return orario; }

    public void setOrario(String orario) { this.orario = orario; }

    public String getTipoStruttura() {
        return tipoStruttura;
    }

    public void setTipoStruttura(String tipoStruttura) {
        this.tipoStruttura = tipoStruttura;
    }

    public String getImageContent() {
        return imageContent;
    }

    public void setImageContent(String imageContent) {
        this.imageContent = imageContent;
    }

    public String getLocalita() {
        return localita;
    }

    public void setLocalita(String localita) {
        this.localita = localita;
    }
}
