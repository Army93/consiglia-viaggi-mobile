package it.cvm.consigliaviaggimobile.models;

public class Recensione {
    private String nomeStruttura;
    private String titolo;
    private String dataRecensione;
    private String dataLastVisited;
    private String proprietario;
    private int valutazione;
    private String commento;

    public Recensione(String nomeStruttura, String titolo, String dataRecensione, String dataLastVisited, String proprietario, int valutazione, String commento) {
        this.nomeStruttura = nomeStruttura;
        this.titolo = titolo;
        this.dataRecensione = dataRecensione;
        this.dataLastVisited = dataLastVisited;
        this.proprietario = proprietario;
        this.valutazione = valutazione;
        this.commento = commento;
    }

    public String getNomeStruttura() {
        return nomeStruttura;
    }

    public void setNomeStruttura(String nomeStruttura) {
        this.nomeStruttura = nomeStruttura;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getDataRecensione() {
        return dataRecensione;
    }

    public void setDataRecensione(String dataRecensione) {
        this.dataRecensione = dataRecensione;
    }

    public String getDataLastVisited() {
        return dataLastVisited;
    }

    public void setDataLastVisited(String dataLastVisited) {
        this.dataLastVisited = dataLastVisited;
    }

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public int getValutazione() {
        return valutazione;
    }

    public void setValutazione(int valutazione) {
        this.valutazione = valutazione;
    }

    public String getCommento() {
        return commento;
    }

    public void setCommento(String commento) {
        this.commento = commento;
    }
}
