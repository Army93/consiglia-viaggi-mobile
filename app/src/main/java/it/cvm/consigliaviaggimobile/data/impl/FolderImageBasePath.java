package it.cvm.consigliaviaggimobile.data.impl;

public enum FolderImageBasePath {
    Ristorante("Ristorante/images/"),
    Attrazione("Attrazione/images/"),
    Albergo("Albergo/images/");

    private String basePath;

    FolderImageBasePath(String basePath){
        this.basePath = basePath;
    }

    public String getBasePath(){
        return this.basePath;
    }
}
