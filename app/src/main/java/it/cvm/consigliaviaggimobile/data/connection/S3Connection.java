package it.cvm.consigliaviaggimobile.data.connection;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.cvm.consigliaviaggimobile.data.impl.FolderImageBasePath;
import it.cvm.consigliaviaggimobile.data.interfaces.IS3;

public class S3Connection extends AsyncTask<Void, Void, List<byte[]>> implements IS3 {

    private static String TAG = S3Connection.class.getSimpleName();

    private static S3Connection instance = null;

    private AmazonS3Client s3Client;
    private Context context;
    private String nomeStruttura;
    private String tipoStruttura;
    private String bucketName = "consiglia-viaggi-data";
    private List<S3ObjectSummary> summary;


    private S3Connection(Context context, String nomeStruttura, String tipoStruttura) {
        this.context = context;
        this.nomeStruttura = nomeStruttura;
        this.tipoStruttura = tipoStruttura;
        this.s3Client = new AmazonS3Client(AWSMobileClient.getInstance());
    }

    /**
     * Ottieni l'istanza di connessione per S3
     * @param context Contesto
     * @return Istanza di connessione per amazon s3
     */
    public static S3Connection getS3Instance(Context context, String nomeStruttura, String tipoStruttura) throws Exception{
        if(instance == null){
                return new S3Connection(context,nomeStruttura, tipoStruttura);
            }
        return instance;
    }



    @Override
    protected List<byte[]> doInBackground(Void... voids) {

        List<byte[]> listImages = new ArrayList<>();

        String path = tipoStruttura.equals("Ristorante") ? FolderImageBasePath.Ristorante.getBasePath().concat(nomeStruttura).concat("/") :
                tipoStruttura.equals("Attrazione") ? FolderImageBasePath.Attrazione.getBasePath().concat(nomeStruttura).concat("/") :
                        tipoStruttura.equals("Albergo") ? FolderImageBasePath.Albergo.getBasePath().concat(nomeStruttura).concat("/") : null;

        ListObjectsV2Request request = new ListObjectsV2Request()
                .withBucketName(bucketName)
                .withPrefix(path)
                .withDelimiter("/");

        summary = s3Client.listObjectsV2(request).getObjectSummaries();
        if(summary.size() == 0 || summary == null){
            Log.e(TAG,"Errore durante l'estrazione delle immagini da S3...");
            return null;
        }

        if(summary.size() == 1){
            Log.w(TAG,"Attenzione non ci sono immagini per la struttura selezionata");

        }else {
            summary.remove(0);
            Log.i(TAG,"Estratte "+summary.size()+" immagini per la struttura selezionata");
            Log.i(TAG,"Procedo con il download delle immagini");

            try {
               listImages  = downloadListImages(summary, path);
            } catch (IOException e) {
                Log.e(TAG,"Errore IOException "+e.getMessage());
                e.printStackTrace();
            }

        }

        return listImages;
    }


    /**
     * Esegue il download delle immagini dal bucket di S3
     * @param summary Lista degli s3ObjectSummary richiesti in precedenza
     * @param path basePath della folder contenente le immagini richieste
     * @return Array buffer contenente le immagini scaricate
     * @throws IOException
     */
    public List<byte[]> downloadListImages(List<S3ObjectSummary> summary, String path) throws IOException {
        Log.i(TAG,"Download delle immagini da S3 relative alla struttura");

        List<byte[]> listaImages = new ArrayList<>();
        Log.i(TAG,"Converto s3objectSummaries in S3Object");

        for (S3ObjectSummary object : summary){
            listaImages.add(IOUtils.toByteArray(s3Client.getObject(bucketName,object.getKey()).getObjectContent()));
        }

        if(listaImages.size() == 0){
            Log.e(TAG,"Errore durante il download delle immagini");
            return null;
        }

        Log.i(TAG,"Download delle immagini completato");
        return listaImages;

    }

}
