package it.cvm.consigliaviaggimobile.data.interfaces;

import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.IOException;
import java.util.List;

public interface IS3 {

    /**
     * Esegue il download delle immagini dal bucket di S3
     * @param summary Lista degli s3ObjectSummary richiesti in precedenza
     * @param path basePath della folder contenente le immagini richieste
     * @return Array buffer contenente le immagini scaricate
     * @throws IOException
     */
    List<byte[]> downloadListImages(List<S3ObjectSummary> summary, String path) throws IOException;

    }
