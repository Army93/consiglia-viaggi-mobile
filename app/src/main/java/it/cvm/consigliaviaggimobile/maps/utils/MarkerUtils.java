package it.cvm.consigliaviaggimobile.maps.utils;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;
import com.example.consigliaviaggimobile.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MarkerUtils  {

   private static List<MarkerOptions> markerOptions = new ArrayList<>();
   private static Map<String,String> tipoStruttura = new HashMap<>();
   private static Map<String, Integer> valutazioneStruttura = new HashMap<>();

    /**
     * Costruisci la lista strutture di markers recuperati da dynamoDB
     * @param lista
     */
    public static void setMarker(List<Document> lista){
        for(Document dbStruttura : lista){

            markerOptions.add(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(Objects.requireNonNull(dbStruttura.get("Latitudine")).asString()), Double.parseDouble(Objects.requireNonNull(dbStruttura.get("Longitudine")).asString())))
                    .title(Objects.requireNonNull(dbStruttura.get("Nome")).asString())
                    .snippet(Objects.requireNonNull(dbStruttura.get("Indirizzo")).asString())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_marker_32)));

            tipoStruttura.put(dbStruttura.get("Nome").asString(), Objects.requireNonNull(dbStruttura.get("Tipo")).asString());
            valutazioneStruttura.put(dbStruttura.get("Nome").asString(), Objects.requireNonNull(dbStruttura.get("Stelle")).asInt());
        }
    }

    /**
     *
     * @return Lista di strutture sottoforma di markers
     */
    public static List<MarkerOptions> getMarkers(){
        return markerOptions;
    }

    /**
     * Ritorna il tipo di struttura in base al nome salvato
     * nella HashMap durante la build dei markers sulla mappa
     * @param nomeStruttura Nome della struttura, coincide con il titolo del marker
     * @return Tipo di struttura
     */
    public static String getTipoStruttura(String nomeStruttura) {
        if(tipoStruttura.containsKey(nomeStruttura)){
            return tipoStruttura.get(nomeStruttura);
        }
        return null;
    }


    /**
     * Ritorna la valutazione della struttura in base al nome salvato
     * nella HashMap durante la build dei markers sulla mappa
     * @param nomeStruttura Nome della struttura, coincide con il titolo del marker
     * @return stelle della struttura
     */
    public static int getValutazioneStruttura(String nomeStruttura) {
        if(valutazioneStruttura.containsKey(nomeStruttura)){
            return valutazioneStruttura.get(nomeStruttura);
        }
        return 0;}

}
