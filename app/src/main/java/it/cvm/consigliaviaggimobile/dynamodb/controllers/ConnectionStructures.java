package it.cvm.consigliaviaggimobile.dynamodb.controllers;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.document.ScanOperationConfig;
import com.amazonaws.mobileconnectors.dynamodbv2.document.Search;
import com.amazonaws.mobileconnectors.dynamodbv2.document.Table;
import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;

import java.util.ArrayList;
import java.util.List;

import it.cvm.consigliaviaggimobile.dynamodb.ConnectionDB;

public class ConnectionStructures {

    private final String TAG = ConnectionStructures.class.getSimpleName();

    private List<String> attributes = new ArrayList<>();

    private static volatile ConnectionDB connectionDB;
    private Table tableName;

    public ConnectionStructures(Context context, String tableName) {
        connectionDB = ConnectionDB.getInstance(context, tableName);

        this.tableName = connectionDB.getDbTable();

        attributes.add("Latitudine");
        attributes.add("Longitudine");
        attributes.add("Nome");
        attributes.add("Indirizzo");
        attributes.add("Stelle");
        attributes.add("Prezzo");
        attributes.add("Descrizione");
        attributes.add("Orario");
        attributes.add("Tipo");
        attributes.add("Image");
        attributes.add("Localita");

    }


    /**
     * Ottieni la lista di tutte le strutture
     *
     * @return List<Document> da convertire in Structures
     */
    public List<Document> getALLStrutture() {
        ScanOperationConfig scanOperationConfig = new ScanOperationConfig();
        scanOperationConfig.withAttributesToGet(attributes);
        Search result = tableName.scan(scanOperationConfig);
        return result.getAllResults();
    }

    /**
     * Recupera la struttura sul db passando le coordinate sottoforma di stringa
     *
     * @param lat Latitudine
     * @param lng longitudine
     * @return Document contenente la lista degli attributi di quella struttura
     */
    public Document getStrutturaByCoordinates(String lat, String lng) {
        List<Document> doc = getALLStrutture();

        if(doc.size() == 0 || doc == null){
            Log.e(TAG, "Lista non accessibile da DB");
            return null;
        }
        for (Document document : doc) {
            if (document.get("Latitudine").asString().equalsIgnoreCase(lat) && document.get("Longitudine").asString().equalsIgnoreCase(lng)) {
                Log.i(TAG, "Struttura presente nel DB");
                return document;
            }
        }
        Log.w(TAG, "Struttura non presente Sul DB");
        return null;
    }

    public void disconnect(){
        connectionDB.closeConnection();
    }
}
