package it.cvm.consigliaviaggimobile.dynamodb.controllers;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.document.ScanOperationConfig;
import com.amazonaws.mobileconnectors.dynamodbv2.document.Search;
import com.amazonaws.mobileconnectors.dynamodbv2.document.Table;
import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;

import java.util.ArrayList;
import java.util.List;

import it.cvm.consigliaviaggimobile.dynamodb.ConnectionDB;
import it.cvm.consigliaviaggimobile.models.Recensione;

public class ConnectionReviews {

    private final String TAG = ConnectionStructures.class.getSimpleName();

    private List<String> attributes  = new ArrayList<>();

    private static volatile ConnectionDB connectionDB;
    private Table tableName;

    public ConnectionReviews(Context context, String tableName){
        connectionDB = ConnectionDB.getInstance(context, tableName);

        this.tableName = connectionDB.getDbTable();

        attributes.add("NomeStruttura");
        attributes.add("NomeUtente");
        attributes.add("Commento");
        attributes.add("DataRecensione");
        attributes.add("DataUltimaVisita");
        attributes.add("Titolo");
        attributes.add("Valutazione");

    }


    /**
     * Ottieni la lista di tutte le Recensioni
     * @return List<Document> da convertire in Structures
     */
    public List<Document> getAllReviews(){
        ScanOperationConfig scanOperationConfig = new ScanOperationConfig();
        scanOperationConfig.withAttributesToGet(attributes);
        Search result = tableName.scan(scanOperationConfig);
        return result.getAllResults();
    }

    /**
     * Ottieni la lista di tutte le Recensioni di quella struttura
     * @param nomeStruttura Ottieni tutte le recensioni di quella struttura
     * @return List<Document> da convertire in Structures
     */
    public List<Document> getAllReviewsFromStructure(String nomeStruttura){
        ScanOperationConfig scanOperationConfig = new ScanOperationConfig();
        scanOperationConfig.withAttributesToGet(attributes);
        Search result = tableName.scan(scanOperationConfig);
        List<Document> lista = result.getAllResults();

        List<Document> listaRecensioni = new ArrayList<>();

        //Se lista vuota ritorna una lista di 0 elements
        if(lista.isEmpty() || lista == null){
            Log.w(TAG,"Lista vuota di commenti per quella struttura");
            return null;
        }

        //Rimuovi tutte le strutture con struttura diversa dal nome passato in input
        for(Document doc : lista){
            if(doc.get("NomeStruttura").asString().equals(nomeStruttura)){
                listaRecensioni.add(doc);
            }
        }

        if(listaRecensioni.size() == 0){
            Log.w(TAG,"Non ho trovato recensioni per quella struttura");
        }else {
            Log.i(TAG,"Ho trovato "+listaRecensioni.size()+" Recensioni per la struttura "+nomeStruttura);
        }

        return listaRecensioni;

    }


    /**
     * Aggiunge la recensione al db
     * @param recensione Recensione processata dall'activity corrispondente
     * @return True se tutto ok, false altrimenti
     */
   public boolean addReview(Recensione recensione){
       Log.i(TAG,"Adding review...");

       Document document = new Document();

       document.put("NomeStruttura",recensione.getNomeStruttura());
       document.put("NomeUtente",recensione.getProprietario());
       document.put("Commento",recensione.getCommento());
       document.put("DataRecensione",recensione.getDataRecensione());
       document.put("DataUltimaVisita",recensione.getDataLastVisited());
       document.put("Titolo",recensione.getTitolo());
       document.put("Valutazione",recensione.getValutazione());

       try {
           tableName.putItem(document);
           return true;
       }catch (Exception e){
           Log.e(TAG,"Errore durante l'inserimento della recensione");
           return false;
       }

   }

   public void disconnect(){
       connectionDB.closeConnection();
   }

}
