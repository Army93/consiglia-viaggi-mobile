package it.cvm.consigliaviaggimobile.dynamodb.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class StringDate {
    static Calendar date;


    /**
     * Genera una stringa secondo il formato DD/MM/YYYY della data
     * ottenuta al momento della richiesta
     * @return Stringa per la data di inserimento della recensione
     */
    public static String generateInstantDate(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();

        return dtf.format(now);
    }

    public static Calendar generateCalendar(){
        date = Calendar.getInstance();
        return date;
    }

    public static Calendar getDate() {
        return date;
    }

    public static int getDay() {
        return date.get(Calendar.DAY_OF_MONTH);
    }


    public static int getMonth() {
        return date.get(Calendar.MONTH);
    }

    public static int getYear() {
        return date.get(Calendar.YEAR);
    }


}
