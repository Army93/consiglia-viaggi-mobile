package it.cvm.consigliaviaggimobile.dynamodb.utils;

public enum StructureType {
    Albergo("Albergo"),
    Ristorante("Ristorante"),
    Attrazione("Attrazione"),
    Alberghi("Alberghi"),
    Ristoranti("Ristoranti"),
    Attrazioni("Attrazioni");


    private final String tipoStruttura;

    StructureType(String tipoStruttura) {
        this.tipoStruttura = tipoStruttura;
    }

    public String getType(){
        return tipoStruttura;
    }
}
