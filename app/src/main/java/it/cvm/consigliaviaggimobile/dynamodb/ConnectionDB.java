package it.cvm.consigliaviaggimobile.dynamodb;

import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.dynamodbv2.document.Table;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

public class ConnectionDB {

    private final String TAG = ConnectionDB.class.getSimpleName();

    private Regions COGNITO_REGION = Regions.EU_WEST_2;
    private CognitoCachingCredentialsProvider credentialsProvider;
    private AmazonDynamoDBClient dbClient;
    private Table dbTable;

    private static volatile ConnectionDB instance;

    private ConnectionDB(Context context, String table){

         //Get Credentials Provider
         credentialsProvider = new CognitoCachingCredentialsProvider(
                 context,
                 "eu-west-2:f992220e-34c9-4716-9c2c-cb5ec365cac7", // Identity pool ID
                 Regions.EU_WEST_2 // Region
         );

         //Get instance of DB
         dbClient = new AmazonDynamoDBClient(credentialsProvider);
         dbClient.setRegion(Region.getRegion(COGNITO_REGION));

         dbTable = Table.loadTable(dbClient, table);

         Log.i(TAG,"dbClient istanziato correttamente");

    }

    /**
     * Ottieni l'istanza della connessione inizializzata del dynamoDB
     * @param context
     * @param tableName Nome della tabella da puntare
     * @return Istanza di connessione a dynamoDB
     */
    public static synchronized ConnectionDB getInstance(Context context, String tableName){
        if(instance == null){
            return new ConnectionDB(context, tableName);
        }
        instance.closeConnection();
        return new ConnectionDB(context, tableName);
    }

    /**
     * Prendi il riferimento della tabelle, potrebbe ritornare null,
     * gestire questo caso all'esterno
     * @return il riferimento alla tabella del DB
     */
    public Table getDbTable() {
        return dbTable;
    }

    public void closeConnection(){
        dbClient.shutdown();
    }

}
