package it.cvm.consigliaviaggimobile.dynamodb.utils;

import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;

import java.util.ArrayList;
import java.util.List;

public class FilterStructuresClass {

    private static final String TAG = FilterStructuresClass.class.getSimpleName();

    /**
     * Costruisce la lista dei ristoranti in base alla lista di tutte le
     * strutture recuperate dal database dynamo
     * @param strutture Lista di tutte le strutture
     * @return Lista di ristoranti
     */
    public static List<Document> getRistoranti(List<Document> strutture){
        List<Document> listRistoranti = new ArrayList<>();

        for (Document ristorante : strutture){
            if (ristorante.get("Tipo").asString().equalsIgnoreCase(StructureType.Ristorante.getType())){
              listRistoranti.add(ristorante);
            }
        }

        if(listRistoranti.size() == 0){
            Log.w(TAG,"Nessun ristorante presente nella lista");
        }
        Log.i(TAG,"Creata lista di ristoranti con size = "+listRistoranti.size());

        return listRistoranti;
    }


    /**
     * Costruisce la lista degli in base alla lista di tutte le
     * strutture recuperate dal database dynamo
     * @param strutture Lista di tutte le strutture
     * @return Lista di alberghi
     */
    public static List<Document> getAlberghi(List<Document> strutture){
        List<Document> listAlberghi = new ArrayList<>();

        for (Document albergo : strutture){
            if (albergo.get("Tipo").asString().equalsIgnoreCase(StructureType.Albergo.getType())){
                listAlberghi.add(albergo);
            }
        }

        if(listAlberghi.size() == 0){
            Log.w(TAG,"Nessun albergo presente nella lista");
        }
        Log.i(TAG,"Creata lista di alberghi con size = "+listAlberghi.size());

        return listAlberghi;
    }

    /**
     * Costruisce la lista delle attrazioni in base alla lista di tutte le
     * strutture recuperate dal database dynamo
     * @param strutture Lista di tutte le strutture
     * @return Lista di attrazioni
     */
    public static List<Document> getAttrazioni(List<Document> strutture){
        List<Document> listAttrazioni = new ArrayList<>();

        for (Document ristorante : strutture){
            if (ristorante.get("Tipo").asString().equalsIgnoreCase(StructureType.Attrazione.getType())){
                listAttrazioni.add(ristorante);
            }
        }

        if(listAttrazioni.size() == 0){
            Log.w(TAG,"Nessuna attrazione presente nella lista");
        }
        Log.i(TAG,"Creata lista di attrazioni con size = "+listAttrazioni.size());

        return listAttrazioni;
    }
}
