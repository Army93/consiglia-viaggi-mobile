package it.cvm.consigliaviaggimobile.activities.searchview;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.consigliaviaggimobile.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.models.SearchBeanStructures;
import it.cvm.consigliaviaggimobile.utils.StructuresUtils;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = SearchActivity.class.getSimpleName();
    private List<SearchBeanStructures> listStructures = new ArrayList<>();
    private SearchTask task;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final SearchView searchView = findViewById(R.id.searchViewStructures);
        final Spinner spinnerPrice = findViewById(R.id.filterPriceSpinner);
        final Spinner spinnerStars = findViewById(R.id.filterStarsSpinner);

        final RecyclerView recyclerView = findViewById(R.id.searchRecycleView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        try {

            //get list Structures
            task = new SearchTask(SearchActivity.this);
            listStructures =task.execute().get();

            final SearchAdapter adapter = new SearchAdapter(listStructures);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);

            //Linea che separa le strutture nella recycler view
            RecyclerView.ItemDecoration divider = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
            recyclerView.addItemDecoration(divider);

            searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

            //Filtra per stelle
            spinnerStars.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    List<SearchBeanStructures> listaFiltrata = StructuresUtils.structuresFilteredByStars(listStructures, position);

                    adapter.setList(listaFiltrata);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //Filtra per fascia di prezzo
            spinnerPrice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    List<SearchBeanStructures> listaFiltrata  = StructuresUtils.structuresFilteredByPrice(listStructures, position);

                    adapter.setList(listaFiltrata);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return false;
                }
            });

        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG,"Errore durante il lancio del task per recuperare le strutture");
            Toast.makeText(SearchActivity.this,"Errore durante il recupero delle strutture per la ricerca",Toast.LENGTH_SHORT).show();
        }

    }

        //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent main = new Intent(SearchActivity.this, MainActivity.class);
        startActivity(main);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent main = new Intent(SearchActivity.this, MainActivity.class);
        startActivity(main);
        finish();
        super.onBackPressed();
    }
}
