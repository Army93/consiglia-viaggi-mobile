package it.cvm.consigliaviaggimobile.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.consigliaviaggimobile.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionStructures;
import it.cvm.consigliaviaggimobile.maps.utils.MarkerUtils;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private static final String TAG = MapsActivity.class.getSimpleName();
    private final String TABLE_NAME = "Strutture";
    private static final int REQUEST_CODE_LOCATION = 100;
    private static final int REQUEST_CODE_COURSELOCATION = 101;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private static final int DEFAULT_ZOOM = 15;

    //Default location is University via Claudio
    private static LatLng mDefaultLocation = new LatLng(40.8285592,14.191157);
    private Location mLastKnownLocation;
    private CameraPosition mCameraPosition;
    private List<MarkerOptions> markersOption;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private PlacesClient placesClient;

    private boolean mPermissionGranted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Retrieve Location and Camera Position from saved instance
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        setContentView(R.layout.activity_maps);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_CODE_LOCATION);
        checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, REQUEST_CODE_COURSELOCATION);


        //Build Places
        Places.initialize(getApplicationContext(),getString(R.string.google_maps_key));
        placesClient = Places.createClient(this);

        //Build fusedLocationClient
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Save position map when app is in pause state
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Build Marker Structure
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
               return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.marker_layout, null);

                TextView title = infoWindow.findViewById(R.id.title);
                title.setText(marker.getTitle());

                TextView snippet = infoWindow.findViewById(R.id.snippet);
                snippet.setText(marker.getSnippet());

                String tipoStruttura = MarkerUtils.getTipoStruttura(marker.getTitle());

                //Setta icona in base al tipo di struttura
                ImageView icon = infoWindow.findViewById(R.id.iconView);
                if(tipoStruttura.equals("Ristorante")){
                    icon.setImageResource(R.drawable.restaurant);
                }else if(tipoStruttura.equals("Attrazione")){
                    icon.setImageResource(R.drawable.attraction);
                }else{
                    icon.setImageResource(R.drawable.hotel);
                }

                //Setta stelle recuperandole dalla hashmap
                RatingBar ratingBar = infoWindow.findViewById(R.id.valutationBar);
                ratingBar.setRating(MarkerUtils.getValutazioneStruttura(marker.getTitle()));

                return infoWindow;
            }
        });

        //Set Action when click on Marker
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.i(TAG,"Entrando nella struttura");
                Intent structureActivity = new Intent(MapsActivity.this,Structure_Activity.class);

                //Passa allo structure activity
                structureActivity.putExtra("latitudine",marker.getPosition().latitude);
                structureActivity.putExtra("longitudine",marker.getPosition().longitude);
                structureActivity.putExtra("Class",MapsActivity.class.getCanonicalName());
                startActivity(structureActivity);
                finish();
            }
        });

        //get device position
        getDeviceLocation();

        updateLocationUI();



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Connect to DynamoDB
        ConnectionStructures connectionStructures = new ConnectionStructures(getApplicationContext(),TABLE_NAME);

        //Costruisco i MarkerOptions per visualizzarli sulla mappa
        MarkerUtils.setMarker(connectionStructures.getALLStrutture());

        markersOption = MarkerUtils.getMarkers();
        if(markersOption.isEmpty() || markersOption == null){
            Log.w(TAG,"Lista Strutture vuota");
        }else{

         //Lista popolata Aggiungi i Markers sulla mappa
            for(MarkerOptions marker : markersOption){
                mMap.addMarker(marker);
            }
            Log.d(TAG,"Markers aggiunti con successo sulla mappa");
         }


    }

    /**
     * Override del metodo, verifica la risposta data quando mostra il popup dei permessi
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == REQUEST_CODE_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MapsActivity.this,
                        "Permesso Geolocalizzazione abilitato",
                        Toast.LENGTH_SHORT)
                        .show();
                mPermissionGranted = true;
            }
            else {
                Toast.makeText(MapsActivity.this,
                        "Permesso Geolocalizzazione Rifiutato",
                        Toast.LENGTH_SHORT)
                        .show();
                mPermissionGranted = false;

                Intent homeActivity = new Intent(MapsActivity.this,MainActivity.class);
                startActivity(homeActivity);
                finish();//Exit activity
            }
        }
        else if (requestCode == REQUEST_CODE_COURSELOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MapsActivity.this,
                        "Permesso Geolocalizzazione Abilitato",
                        Toast.LENGTH_SHORT)
                        .show();
                mPermissionGranted = true;
            }
            else {
                Toast.makeText(MapsActivity.this,
                        "Permesso Geolocalizzazione Rifiutato",
                        Toast.LENGTH_SHORT)
                        .show();
                mPermissionGranted=false;

                Intent homeActivity = new Intent(MapsActivity.this,MainActivity.class);
                startActivity(homeActivity);
                finish();//Exit activity

            }
        }
    }

    /**
     * Verifica se i permessi sono enabled
     * @param permission
     * @param requestCode
     */
    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(MapsActivity.this, permission)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[] { permission },
                    requestCode);
        }
        else {
            Toast.makeText(MapsActivity.this,
                    "Permesso Geolocalizzazione già abilitato",
                    Toast.LENGTH_SHORT)
                    .show();
            mPermissionGranted = true;
        }
    }

    /**
     * Ottiene la posizione del dispositivo
     */
    private void getDeviceLocation(){
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */


        try {
            Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        Location mLastKnownLocation = task.getResult();

                        if (mLastKnownLocation != null) {

                            LatLng updatedLocation = new LatLng(mLastKnownLocation.getLatitude(),
                                    mLastKnownLocation.getLongitude());

                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    updatedLocation, DEFAULT_ZOOM));

                            //Aggiunge il raggio di azione del cerchio
                            mMap.addCircle(new CircleOptions()
                                    .center(updatedLocation)
                                    .radius(500.00)
                                    .strokeColor(Color.CYAN)
                                    .strokeWidth(3F)
                                    .fillColor(Color.argb(100,50,169,223)));

                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        Log.e(TAG, "Exception: %s", task.getException());
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });

        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Update Location UI
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent mainActivity = new Intent(MapsActivity.this, MainActivity.class);
        startActivity(mainActivity);
        finish();
        return super.onSupportNavigateUp();
    }

    //pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent mainActivity = new Intent(MapsActivity.this, MainActivity.class);
        startActivity(mainActivity);
        finish();
        super.onBackPressed();
    }

}
