package it.cvm.consigliaviaggimobile.activities.Login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignInResult;
import com.example.consigliaviaggimobile.R;

import androidx.appcompat.app.AppCompatActivity;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.activities.Login.ManagePassword.ResetPasswordActivity;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final Button submitLogin = findViewById(R.id.submitLoginButton);

        final EditText usernameText = findViewById(R.id.usernameText);
        final EditText passwordText = findViewById(R.id.pswText);

        final TextView registerText = findViewById(R.id.registerText);
        final TextView forgotPswText = findViewById(R.id.forgotPasswordText);

        //Avvia la fase di login quando si clicca sul Button Login
        submitLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();

                //Verifica che tutti i campi siano stati inseriti
                if (username.isEmpty() || password.isEmpty()) {

                    Toast.makeText(getApplicationContext(), "Inserire tutti i campi richiesti", Toast.LENGTH_LONG).show();
                }

                //Avvia la fase di verifica utente
                else {

                    AWSMobileClient.getInstance().signIn(username, password, null, new Callback<SignInResult>() {
                        @Override
                        public void onResult(final SignInResult result) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG,"Verifico stato sign-in");
                                    switch (result.getSignInState()){
                                        case DONE : Toast.makeText(getApplicationContext(),"Login Effettuato con Successo",Toast.LENGTH_SHORT).show();
                                            Intent home = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(home);
                                            finish();
                                        break;

                                        case PASSWORD_VERIFIER: Toast.makeText(getApplicationContext(),"Password ancora da verificare",Toast.LENGTH_SHORT).show(); break;
                                        case DEVICE_SRP_AUTH: Toast.makeText(getApplicationContext(),"Device SRP auth",Toast.LENGTH_SHORT).show(); break;
                                        case DEVICE_PASSWORD_VERIFIER: Toast.makeText(getApplicationContext(),"Device Password verifier",Toast.LENGTH_SHORT).show(); break;
                                        case ADMIN_NO_SRP_AUTH: Toast.makeText(getApplicationContext(),"Admin no srp auth",Toast.LENGTH_SHORT).show(); break;
                                        case UNKNOWN: Toast.makeText(getApplicationContext(),"Unkownk",Toast.LENGTH_SHORT).show(); break;
                                        default: Toast.makeText(getApplicationContext(),"Errore sconosciuto",Toast.LENGTH_SHORT).show(); break;
                                    }
                                }
                            });
                        }

                        @Override
                        public void onError(Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG,"Errore durante la fase di login, credenziali errate");
                                    Toast.makeText(getApplicationContext(),"Credenziali Errate",Toast.LENGTH_LONG).show();
                                }
                            });

                        }
                    });
                }
            }
        });

        //Password dimenticata -> passa all'activity per il recupero password
        forgotPswText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resetActivity = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                resetActivity.putExtra("username",usernameText.getText().toString());
                startActivity(resetActivity);
                finish();
            }
        });

        //Passa alla Registrazione di un nuovo utente
        registerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerActivity = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(registerActivity);
                finish();
            }
        });
    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent loginActivity = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(loginActivity);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent loginActivity = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(loginActivity);
        finish();
        super.onBackPressed();
    }
}
