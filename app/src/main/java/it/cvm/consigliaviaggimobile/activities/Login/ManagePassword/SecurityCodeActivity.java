package it.cvm.consigliaviaggimobile.activities.Login.ManagePassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.ForgotPasswordResult;
import com.example.consigliaviaggimobile.R;

import androidx.appcompat.app.AppCompatActivity;
import it.cvm.consigliaviaggimobile.activities.Login.LoginActivity;

public class SecurityCodeActivity extends AppCompatActivity {

    private final String TAG= SecurityCodeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_code);

        final EditText verificationCodeText = findViewById(R.id.verificationLostPassword);

        final EditText pswUpdate = findViewById(R.id.newPasswordText);

        Button sendCodButton = findViewById(R.id.sendCodButton);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String username = getIntent().getStringExtra("username");
        if(username == null){
            Log.w(TAG,"Username non passato dall'intent precedente");

        }


        //Quando si clicca sul tasto continua parte il processo di verifica dati
        sendCodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Inizio processo di verifica codice di sicurezza e nuova password");
                //Verifica che sia stato scritto il codice
                 if(verificationCodeText.getText().toString().isEmpty() || pswUpdate.getText().toString().isEmpty()){
                    Log.e(TAG,"Campi non compilati correttamente");
                    Toast.makeText(getApplicationContext(),"Campi richiesti non compilati",Toast.LENGTH_LONG).show();
                }else {
                    Log.i(TAG,"Verifico Codice di sicurezza");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Verifica che password e codice di sicurezza siano corretti
                            AWSMobileClient.getInstance().confirmForgotPassword(pswUpdate.getText().toString(), verificationCodeText.getText().toString(), new Callback<ForgotPasswordResult>() {
                                @Override
                                public void onResult(ForgotPasswordResult result) {

                                    switch (result.getState()){
                                        case DONE :
                                            Log.i(TAG,"Password cambiata correttamente");
                                            Toast.makeText(getApplicationContext(),"Password aggiornata correttamente",Toast.LENGTH_LONG).show();
                                            Intent loginIntent = new Intent(SecurityCodeActivity.this, LoginActivity.class);
                                            startActivity(loginIntent);
                                            finish();
                                            break;

                                        default: Log.e(TAG,"ForgotPasswordState status non supportato");
                                        break;
                                    }
                                }

                                @Override
                                public void onError(Exception e) {
                                    Log.e(TAG,"Errore durante la verifica della password o del codice di sicurezza");
                                }
                            });
                        }
                    });
                }
            }
        });


    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent loginIntent = new Intent(SecurityCodeActivity.this, ResetPasswordActivity.class);
        startActivity(loginIntent);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent loginIntent = new Intent(SecurityCodeActivity.this, ResetPasswordActivity.class);
        startActivity(loginIntent);
        finish();
        super.onBackPressed();
    }
}
