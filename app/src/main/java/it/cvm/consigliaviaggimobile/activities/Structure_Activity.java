package it.cvm.consigliaviaggimobile.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;
import com.example.consigliaviaggimobile.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.activities.Login.LoginActivity;
import it.cvm.consigliaviaggimobile.activities.recycleview.ImageStructureAdapter;
import it.cvm.consigliaviaggimobile.activities.recycleview.ReviewAdapter;
import it.cvm.consigliaviaggimobile.data.LoadingDialog;
import it.cvm.consigliaviaggimobile.data.connection.S3Connection;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionReviews;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionStructures;
import it.cvm.consigliaviaggimobile.models.Struttura;
import it.cvm.consigliaviaggimobile.utils.ReviewsUtils;
import it.cvm.consigliaviaggimobile.utils.StructuresUtils;

public class Structure_Activity extends AppCompatActivity {

    private final String TAG = Structure_Activity.class.getSimpleName();

    private final String TABLE_NAME = "Strutture";
    private final String TABLE_REVIEW ="Recensioni";

    private static Class<?> reflectClass;
    private Struttura struttura = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_structure_);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String classePrev = getIntent().getStringExtra("Class");
        final double latitudine = getIntent().getDoubleExtra("latitudine",0);
        final double longitudine = getIntent().getDoubleExtra("longitudine",0);

        if(classePrev != null){
            try {
                reflectClass = Class.forName(classePrev);
            } catch (ClassNotFoundException e) {
                Log.e(TAG,"Errore durante la get della classe chiamante");
                Intent home = new Intent(Structure_Activity.this, MainActivity.class);
                startActivity(home);
                finish();
            }
        }

        //Se non vengono passati gli argomenti della struttura, mostra errore
        if(latitudine == 0 || longitudine == 0){
            Log.e(TAG, "Errore, lat e lng non passati dall'activity precedente");
            Toast.makeText(getApplicationContext(),"Errore durante il recupero della struttura",Toast.LENGTH_SHORT).show();
            Intent mapsActivity = new Intent(Structure_Activity.this , reflectClass);

            startActivity(mapsActivity);
            finish();
        }

            new ConnectStructure().execute(latitudine, longitudine);

    }


    public class ConnectStructure extends AsyncTask<Double, Void, Void>{
        final TextView nomeStruttura = findViewById(R.id.nameStrutturaText);
        final TextView indirizzoStruttura = findViewById(R.id.indirizzoText);
        final RatingBar ratingStar= findViewById(R.id.barStars);
        final TextView prezzoStruttura = findViewById(R.id.costoText);
        final TextView descrizioneStruttura = findViewById(R.id.descrizioneStructureText);
        final TextView orarioStruttura = findViewById(R.id.orarioText);
        final ConstraintLayout panoramicLayout = findViewById(R.id.constraintLayoutPanoramica);
        final ConstraintLayout reviewsLayout = findViewById(R.id.constraintLayoutRecensioni);
        final ViewPager imageStructure = findViewById(R.id.imageStructure);
        final TabLayout tabLayout = findViewById(R.id.tabStructuresLayout);


        LoadingDialog dialog = new LoadingDialog(Structure_Activity.this);
        S3Connection s3Connection;

        Document retrieve = null;
        ConnectionStructures connectionStructures;

        double lat;
        double lng;

        @Override
        protected Void doInBackground(Double... doubles) {
            //Recupera struttura

            Double[] params = doubles;
            lat = params[0];
            lng = params[1];

            connectionStructures = new ConnectionStructures(Structure_Activity.this, TABLE_NAME);
            retrieve = connectionStructures.getStrutturaByCoordinates(String.valueOf(lat), String.valueOf(lng));

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(Objects.isNull(retrieve)){
                dialog.startLoadingDialog();
            }

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(retrieve == null){
                Log.e(TAG,"Errore non sono riuscito a recuperare la struttura dal DB");
                Intent home = new Intent(Structure_Activity.this, MainActivity.class);
                startActivity(home);
                finish();
            }

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if(tab.getText().toString().equalsIgnoreCase("Panoramica")){
                        panoramicLayout.setVisibility(View.VISIBLE);
                        reviewsLayout.setVisibility(View.INVISIBLE);
                        cancel(true);
                    }else {
                        panoramicLayout.setVisibility(View.INVISIBLE);
                        reviewsLayout.setVisibility(View.VISIBLE);
                        new ConnectReview().execute(struttura.getNome(),String.valueOf(struttura.getLatitudine()),String.valueOf(struttura.getLongitudine()));
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            struttura = new Struttura(Double.parseDouble(Objects.requireNonNull(retrieve.get("Latitudine")).asString()),
                    Double.parseDouble(Objects.requireNonNull(retrieve.get("Longitudine")).asString()),
                    Objects.requireNonNull(retrieve.get("Nome")).asString(),
                    Objects.requireNonNull(retrieve.get("Indirizzo")).asString(),
                    Objects.requireNonNull(retrieve.get("Stelle")).asInt(),
                    Objects.requireNonNull(retrieve.get("Prezzo")).asFloat(),
                    Objects.requireNonNull(retrieve.get("Descrizione")).asString(),
                    Objects.requireNonNull(retrieve.get("Orario")).asString(),
                    Objects.requireNonNull(retrieve.get("Tipo").asString()),
                    Objects.requireNonNull((retrieve.get("Localita").asString())));

            //Popolo i campi della struttura Panoramica
            nomeStruttura.setText(struttura.getNome());
            indirizzoStruttura.setText(struttura.getIndirizzo());
            ratingStar.setIsIndicator(true);
            ratingStar.setRating(struttura.getStelle());
            prezzoStruttura.setText(StructuresUtils.getFasciaByPrice(struttura.getPrezzoMedio()));
            descrizioneStruttura.setText(struttura.getDescrizione());
            orarioStruttura.setText(struttura.getOrario());

            try {
                s3Connection = S3Connection.getS3Instance(Structure_Activity.this, struttura.getNome(), struttura.getTipoStruttura());
                List<byte[]> listImages = null;

                //Procedo al download delle immagini

                    listImages = s3Connection.execute().get();

                //Lista immagini vuota
                if(listImages.size() == 0 || listImages == null){
                    Log.e(TAG,"Errore durante l'estrazione delle immagini");
                    Toast.makeText(Structure_Activity.this,"Errore durante l'estrazione delle immagini",Toast.LENGTH_SHORT).show();
                }
                else {
                    //Metti la lista nel pageViewer
                    ImageStructureAdapter imageAdapter = new ImageStructureAdapter(Structure_Activity.this, listImages);
                    imageStructure.setAdapter(imageAdapter);
                }
            } catch (Exception e) {
               Log.e(TAG,"Errore, non sono riuscito a recuperare le credenziali di cognito");
               Toast.makeText(Structure_Activity.this,"Errore: credenziali cognito non valide",Toast.LENGTH_LONG).show();
            }

            dialog.dismissDialog();

        }
    }

    public class ConnectReview extends AsyncTask<String, Void, Void>{

        ConnectionReviews connectionReviews;
        List<Document> listRecensioni;

        final Button writeReview = findViewById(R.id.writeReviewButton);
        final Spinner spinnerMonth = findViewById(R.id.filterMonthSpinner);
        final Spinner spinnerRate = findViewById(R.id.filterRateSpinner);
        RecyclerView recyclerView = findViewById(R.id.recycleView);

        RecyclerView.Adapter mAdapter;

        double lat;
        double lng;

        @Override
        protected Void doInBackground(String... strings) {
            //Ottieni tutte le recensioni di quella struttura
             connectionReviews = new ConnectionReviews(Structure_Activity.this, TABLE_REVIEW);
             listRecensioni = connectionReviews.getAllReviewsFromStructure(strings[0]);

             lat = Double.parseDouble(strings[1]);
             lng = Double.parseDouble(strings[2]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(listRecensioni == null || listRecensioni.size() == 0){
                Toast.makeText(Structure_Activity.this,"Non sono presenti recensioni per questa struttura",Toast.LENGTH_SHORT).show();
            }
            else {
                //Fai partire il thread per la visualizzazione delle recensioni

                        RecyclerView.LayoutManager layoutManager;

                        // in content do not change the layout size of the RecyclerView
                        recyclerView.setHasFixedSize(true);

                        // use a linear layout manager
                        layoutManager = new LinearLayoutManager(Structure_Activity.this);
                        recyclerView.setLayoutManager(layoutManager);

                        // specify an adapter (see also next example)


                        mAdapter = new ReviewAdapter(listRecensioni);
                        recyclerView.setAdapter(mAdapter);

            }

            //Scrivi recensione
            writeReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AWSMobileClient.getInstance().initialize(getApplicationContext(), new Callback<UserStateDetails>() {
                        @Override
                        public void onResult(final UserStateDetails result) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.i("UserDetails", "Stato User: " + result.getUserState().toString());
                                    switch (result.getUserState()) {
                                        case SIGNED_IN:
                                            if(!ReviewsUtils.checkIfReviewExists(listRecensioni,AWSMobileClient.getInstance().getUsername())) {
                                                Log.i("UserDetails", "SIGNED_IN " + AWSMobileClient.getInstance().getUsername());
                                                Intent writeActivity = new Intent(Structure_Activity.this, WriteReviewActivity.class);
                                                finish();
                                                writeActivity.putExtra("Nome", struttura.getNome());
                                                writeActivity.putExtra("Latitudine", lat);
                                                writeActivity.putExtra("Longitudine", lng);
                                                startActivity(writeActivity);

                                            }else {
                                                new AlertDialog.Builder(Structure_Activity.this)
                                                        .setTitle("Attenzione")
                                                        .setMessage("Recensione già presente per questa struttura con utente: "+AWSMobileClient.getInstance().getUsername())
                                                        .setPositiveButton("OK", null).create().show();
                                            }
                                            break;
                                        case SIGNED_OUT:
                                            new AlertDialog.Builder(Structure_Activity.this)
                                                    .setTitle("Attenzione")
                                                    .setMessage("Devi prima effettuare il login")
                                                    .setNegativeButton(android.R.string.no, null)
                                                    .setPositiveButton("Vai al login", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                            Intent loginActivity = new Intent(Structure_Activity.this , LoginActivity.class);
                                                            startActivity(loginActivity);
                                                            finish();
                                                        }
                                                    }).create().show();
                                            break;
                                        default:
                                            AWSMobileClient.getInstance().signOut();
                                            Log.i("UserDetails", "Default Status");
                                            break;
                                    }
                                }
                            });
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
                }
            });


            spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<Document> listaFiltrata = new ArrayList<>(listRecensioni);

                    listaFiltrata = ReviewsUtils.filtraReviewByMonth(listRecensioni,position);
                    if(listaFiltrata.size() == 0) {
                        Toast.makeText(Structure_Activity.this, "Nessuna recensione per mese selezionato", Toast.LENGTH_SHORT).show();
                    }

                    recyclerView.setAdapter(new ReviewAdapter(listaFiltrata));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spinnerRate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<Document> listaFiltrata = new ArrayList<>(listRecensioni);

                    listaFiltrata = ReviewsUtils.filtraReviewByRating(listRecensioni,position);
                    if(listaFiltrata.size() == 0) {
                        Toast.makeText(Structure_Activity.this, "Nessuna recensione trovate con "+position+" stelle", Toast.LENGTH_SHORT).show();
                    }

                    recyclerView.setAdapter(new ReviewAdapter(listaFiltrata));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent maps = new Intent(Structure_Activity.this, reflectClass);
        startActivity(maps);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent maps = new Intent(Structure_Activity.this, reflectClass);
        startActivity(maps);
        finish();
        super.onBackPressed();
    }
}
