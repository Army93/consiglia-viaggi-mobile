package it.cvm.consigliaviaggimobile.activities.searchview;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;

import java.util.ArrayList;
import java.util.List;

import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionStructures;
import it.cvm.consigliaviaggimobile.models.SearchBeanStructures;

public class SearchTask extends AsyncTask<Void, Void, List<SearchBeanStructures>> {

    private static final String TAG = SearchTask.class.getSimpleName();

        final String TABLE_NAME = "Strutture";
        Context context;
        ConnectionStructures dbStructures;

    public SearchTask(Context context) {
        this.context = context;
    }

    List<SearchBeanStructures> listaStrutture = new ArrayList<>();

        @Override
        protected List<SearchBeanStructures> doInBackground(Void... voids) {

            dbStructures = new ConnectionStructures(context ,TABLE_NAME);

            //Operazioni di estrazione di tutte le strutture
            final List<Document> allStrutture = dbStructures.getALLStrutture();

            if(allStrutture.isEmpty()){
                Log.w(TAG,"Non vi sono Strutture presenti nel DB");
            }

            //Popola la lista dei nomi delle strutture e degli object recuperate con i 3 attributi che la identificano
            for (Document nome : allStrutture) {
                listaStrutture.add(new SearchBeanStructures(
                        nome.get("Nome").asString(),
                        Double.parseDouble(nome.get("Latitudine").asString()),
                        Double.parseDouble(nome.get("Longitudine").asString()),
                        nome.get("Localita").asString(),
                        nome.get("Stelle").asInt(),
                        nome.get("Prezzo").asFloat()
                ));
            }

            return listaStrutture;
        }
    }
