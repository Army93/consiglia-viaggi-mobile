package it.cvm.consigliaviaggimobile.activities.recycleview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class ImageStructureAdapter extends PagerAdapter {

    private Context context;
    private List<byte[]> imagesList;

    public ImageStructureAdapter(Context context, List<byte[]> listImages){
        this.context = context;
        this.imagesList = listImages;
    }

    @Override
    public int getCount() {
        return imagesList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        Bitmap bitmap = BitmapFactory.decodeByteArray(imagesList.get(position), 0, imagesList.get(position).length);
        imageView.setImageBitmap(bitmap);

        container.addView(imageView,0);
        return imageView;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }
}
