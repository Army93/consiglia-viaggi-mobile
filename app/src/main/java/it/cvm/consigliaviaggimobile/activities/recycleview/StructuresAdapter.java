package it.cvm.consigliaviaggimobile.activities.recycleview;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;
import com.example.consigliaviaggimobile.R;

import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.activities.Structure_Activity;
import it.cvm.consigliaviaggimobile.utils.StructuresUtils;

public class StructuresAdapter extends RecyclerView.Adapter<StructuresAdapter.MyViewHolder> {

    private final String TAG = StructuresAdapter.class.getSimpleName();

    private List<Document> mDataset;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ConstraintLayout textView;

        public MyViewHolder(ConstraintLayout v) {
            super(v);
            textView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public StructuresAdapter(List<Document> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public StructuresAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_structures_layout, parent, false);


        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        ImageView iconView = (ImageView) holder.textView.findViewById(R.id.imageStructureView);
        TextView nomeStruttura = (TextView) holder.textView.getViewById(R.id.titleStructureTextlist);
        TextView prezzoStruttura = (TextView) holder.textView.getViewById(R.id.prezzoStrutturaText);
        RatingBar ratingBar = (RatingBar) holder.textView.getViewById(R.id.starsListStructureRating);


        //Popola il layout con i dati presi da DynamoDB
        ratingBar.setRating(mDataset.get(position).get("Stelle").asInt());
        ratingBar.setIsIndicator(true);

        nomeStruttura.setText(mDataset.get(position).get("Nome").asString());
        prezzoStruttura.setText(StructuresUtils.getFasciaByPrice(mDataset.get(position).get("Prezzo").asFloat()));

        String tipoStruttura = mDataset.get(position).get("Tipo").asString();

        if(tipoStruttura.equals("Ristorante")){
            iconView.setImageResource(R.drawable.restaurant);
        }else if(tipoStruttura.equals("Attrazione")){
            iconView.setImageResource(R.drawable.attraction);
        }else {
            iconView.setImageResource(R.drawable.hotel);
        }

        //Click per passare alla struttura
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Cliccato vai alla struttura, preparo l'activity della struttura");

                Context context = v.getContext();
                Intent structure = new Intent(context, Structure_Activity.class);
                structure.putExtra("longitudine",Double.parseDouble(mDataset.get(position).get("Longitudine").asString()));
                structure.putExtra("latitudine",Double.parseDouble(mDataset.get(position).get("Latitudine").asString()));
                structure.putExtra("Class", MainActivity.class.getCanonicalName());
                context.startActivity(structure);

            }
        });


    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

