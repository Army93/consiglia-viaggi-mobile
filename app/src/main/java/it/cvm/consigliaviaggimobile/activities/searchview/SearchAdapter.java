package it.cvm.consigliaviaggimobile.activities.searchview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.consigliaviaggimobile.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.activities.Structure_Activity;
import it.cvm.consigliaviaggimobile.models.SearchBeanStructures;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> implements Filterable {

    private final String TAG = SearchAdapter.class.getSimpleName();

    private List<SearchBeanStructures> listStructures;
    private List<SearchBeanStructures> listFull;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ConstraintLayout textView;

        public MyViewHolder(ConstraintLayout v) {
            super(v);
            textView = v;
        }
    }

    public SearchAdapter(List<SearchBeanStructures> beans){
        this.listStructures = beans;
        this.listFull = new ArrayList<>(beans);

    }

    @NonNull
    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_suggestion_layout, parent, false);

        SearchAdapter.MyViewHolder vh = new SearchAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.MyViewHolder holder, int position) {
        final SearchBeanStructures currentItem = listStructures.get(position);
        final TextView text = holder.textView.findViewById(R.id.nameStrutturaSearch);
        final TextView cityName = holder.textView.findViewById(R.id.cityNameText);

        text.setTextColor(Color.parseColor("#333333"));
        text.setText(currentItem.getNome());
        cityName.setText(currentItem.getLocalita());

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Struttura trovata, passo alla struttura: "+currentItem.getNome());
                Context context = v.getContext();
                Intent structureActivity = new Intent(context, Structure_Activity.class);
                structureActivity.putExtra("longitudine",currentItem.getLongitudine());
                structureActivity.putExtra("latitudine",currentItem.getLatitudine());
                structureActivity.putExtra("Class", MainActivity.class.getCanonicalName());
                listFull.clear();
                listStructures.clear();
                context.startActivity(structureActivity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listStructures.size();
    }

    @Override
    public Filter getFilter() {
        return listFilter;
    }

    private Filter listFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<SearchBeanStructures> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length()==0){
                filteredList.addAll(listFull);
            }else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                //Search for name or localita
                for(SearchBeanStructures structure : listFull){
                    if(structure.getNome().toLowerCase().contains(filterPattern) || structure.getLocalita().toLowerCase().contains(filterPattern)){
                       filteredList.add(structure);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listStructures.clear();
            listStructures.addAll((List)results.values);

            notifyDataSetChanged();
        }
    };

    public void setList(List<SearchBeanStructures> listFilter){
        this.listStructures = listFilter;
    }
}

