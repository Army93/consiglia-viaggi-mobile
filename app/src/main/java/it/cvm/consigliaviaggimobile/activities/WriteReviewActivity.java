package it.cvm.consigliaviaggimobile.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.example.consigliaviaggimobile.R;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import it.cvm.consigliaviaggimobile.MainActivity;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionReviews;
import it.cvm.consigliaviaggimobile.dynamodb.utils.StringDate;
import it.cvm.consigliaviaggimobile.models.Recensione;
import it.cvm.consigliaviaggimobile.utils.UserTask;

public class WriteReviewActivity extends AppCompatActivity {

    private final String TAG = WriteReviewActivity.class.getSimpleName();
    private final String TABLE_NAME = "Recensioni";
    private double longitudine;
    private double latitudine;

    //Dialog calendario
    private DatePickerDialog.OnDateSetListener dateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Button sendReview = findViewById(R.id.sendReviewButton);

        final RatingBar ratingBar = findViewById(R.id.ratingBar);
        final Switch nicknameSwitch = findViewById(R.id.switchNickName);
        final EditText titoloRecensione = findViewById(R.id.titoloReviewText);
        final EditText commentReview = findViewById(R.id.commentReviewText);

        final String nomeStruttura = getIntent().getStringExtra("Nome");

        final String username = AWSMobileClient.getInstance().getUsername();
        final TextView mDisplayDate = findViewById(R.id.dataVisit);

        latitudine = getIntent().getDoubleExtra("Latitudine",0);
        longitudine = getIntent().getDoubleExtra("Longitudine",0);

        //Dialog calendario
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                //Ottieni data attuale
                final int anno = cal.get(Calendar.YEAR);
                final int mese = cal.get(Calendar.MONTH);
                final int giorno = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(WriteReviewActivity.this, R.style.DatePickerTheme, dateSetListener, anno, mese, giorno);

                //nasconde il giorno
                dialog.getDatePicker().findViewById(getResources().getIdentifier("day","id","android")).setVisibility(View.GONE);

                //Mostra come data recente il giorno corrente e come data più vecchia 11 mesi precedenti
                cal.set(Calendar.DAY_OF_MONTH, 17);
                dialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
                Log.i(TAG, "Time: " + cal.getTime());
                Log.i(TAG, "TimeInMillis: " + cal.getTimeInMillis());

                cal.add(Calendar.MONTH, -11); // mostra solo gli ultimi 12 mesi
                dialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                Log.i(TAG, "Time (after min): " + cal.getTime());

                //Text datepicker non editabile
                dialog.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int i2) {

                month = month + 1;
                String month_name = Month.of(month).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ITALIAN);

                mDisplayDate.setText(month_name + " " + year);
                Log.i(TAG, "Inserita la data " + mDisplayDate.getText().toString());
            }
        };

        //Parametri non passati, Recensione impossibile da effettuare
        if(nomeStruttura.isEmpty() || nomeStruttura == null || latitudine == 0 || longitudine == 0){
            Log.e(TAG,"Errore, parametri non passati correttamente dall'activity precedente");
            Intent home = new Intent(this,MainActivity.class);
            startActivity(home);
            finish();
        }

        //Non loggato, impossibile mandare richiesta
        if(username.isEmpty() || username == null){
            Log.e(TAG,"Errore, utente non loggato, impossibile inviare recensione");
            Toast.makeText(getApplicationContext(),"Utente non loggato per effettuare la recensione",Toast.LENGTH_LONG).show();
            Intent home = new Intent(this,MainActivity.class);
            startActivity(home);
            finish();
        }

        final int[] stelle = new int[1];

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                stelle[0] = (int)rating;
                Log.i(TAG,"Hai selezionato "+stelle[0]+" stelle");
            }
        });

        //Invia recensione
        sendReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Se campi non compilati
                if(stelle[0] == 0 || mDisplayDate.getText().toString().isEmpty() ||
                        titoloRecensione.getText().toString().isEmpty() || commentReview.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Inserire tutti i campi",Toast.LENGTH_LONG).show();
                    Log.e(TAG,"Errore: non sono stati inseriti tutti i campi richiesti");
                }
                else {
                    //Prepara i dati per Dynamo
                    Log.i(TAG, "Preparo richiesta per DynamoDB");

                    //Genero data della richiesta
                    String dataCorrente = StringDate.generateInstantDate();
                    String owner = username;

                    //If Enabled show name and surname
                    if(nicknameSwitch.isChecked()) {
                        try {

                        Log.i(TAG,"Estraggo nome e cognome utente");

                        UserTask task = new UserTask();
                        owner = task.execute().get();

                        } catch (Exception e) {
                            Log.e(TAG,"Errore durante l'estrazione degli attributi nome e cognome dell'utente");
                            }
                        }
                            Recensione recensione = new Recensione(nomeStruttura,
                                    titoloRecensione.getText().toString(),
                                    dataCorrente,
                                    mDisplayDate.getText().toString(),
                                    owner,
                                    stelle[0],
                                    commentReview.getText().toString()
                            );
                            //Avvio il processo in Background
                            new writeReview().execute(recensione);
                    }

                }
            });
    }



    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        new AlertDialog.Builder(this)
                .setTitle("Sei sicuro?")
                .setMessage("Uscendo annullerai l'invio della recensione")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent home = new Intent(WriteReviewActivity.this, Structure_Activity.class);
                        home.putExtra("longitudine",longitudine);
                        home.putExtra("latitudine",latitudine);
                        home.putExtra("Class",MainActivity.class.getCanonicalName());
                        startActivity(home);
                        finish();
                    }
                }).create().show();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar + avviso annullamento operazione
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Sei sicuro?")
                .setMessage("Uscendo annullerai l'invio della recensione")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent home = new Intent(WriteReviewActivity.this, Structure_Activity.class);
                        home.putExtra("longitudine",longitudine);
                        home.putExtra("latitudine",latitudine);
                        home.putExtra("Class",MainActivity.class.getCanonicalName());
                        startActivity(home);
                        finish();
                        WriteReviewActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    public class writeReview extends android.os.AsyncTask<Recensione, Void, Void> {
        Recensione recensione;
        boolean result;

        @Override
        protected Void doInBackground(Recensione... recensioni) {

            if(recensioni[0] == null){
                Log.e(TAG,"Errore durante il passaggio della recensione al processo in Background");
                Intent mainActivities = new Intent(WriteReviewActivity.this, MainActivity.class);
                startActivity(mainActivities);
                finish();
            }

            recensione = recensioni[0];

            //Creo Connessione e invio a dynamo
            ConnectionReviews connectionReviews = new ConnectionReviews(WriteReviewActivity.this,TABLE_NAME);

            if(connectionReviews.addReview(recensione)){
                Log.i(TAG,"Recensione inserita con successo...");
                result = true;
            } else {
                    Log.i(TAG,"Recensione non inserita!");
                    result =false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //Non andato a buon fine
            if(!result){
                Toast.makeText(getApplicationContext(),"Errore, Recensione non inserita",Toast.LENGTH_SHORT).show();

                Intent home = new Intent(WriteReviewActivity.this, Structure_Activity.class);
                home.putExtra("latitudine",latitudine);
                home.putExtra("longitudine",longitudine);
                home.putExtra("Class",MainActivity.class.getCanonicalName());
                startActivity(home);
                finish();
            }
            else {
                Toast.makeText(WriteReviewActivity.this,"Recensione inserita con successo",Toast.LENGTH_SHORT).show();

                Intent home = new Intent(WriteReviewActivity.this, Structure_Activity.class);
                home.putExtra("longitudine",longitudine);
                home.putExtra("latitudine",latitudine);
                home.putExtra("Class",MainActivity.class.getCanonicalName());
                startActivity(home);
                finish();
            }
        }
    }

}


