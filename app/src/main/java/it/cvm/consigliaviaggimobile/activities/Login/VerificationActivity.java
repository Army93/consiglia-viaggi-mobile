
package it.cvm.consigliaviaggimobile.activities.Login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignUpResult;
import com.example.consigliaviaggimobile.R;

import androidx.appcompat.app.AppCompatActivity;
import it.cvm.consigliaviaggimobile.MainActivity;

public class VerificationActivity extends AppCompatActivity {

    private final String TAG = VerificationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        final EditText usernameInput = findViewById(R.id.usernameVerificationText);
        final EditText verificationCodeInput = findViewById(R.id.verificationCodeText);
        final Button sendVerification = findViewById(R.id.submitVerificationButton);

        usernameInput.setText(getIntent().getStringExtra("username"));

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Invia codice di sicurezza per completare la fase di login
        sendVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String codeVerification = verificationCodeInput.getText().toString();

                //Verifica codice di sicurezza
                AWSMobileClient.getInstance().confirmSignUp(usernameInput.getText().toString(), codeVerification, new Callback<SignUpResult>() {
                    @Override
                    public void onResult(SignUpResult result) {

                        if(!result.getConfirmationState()){
                            Toast.makeText(VerificationActivity.this,"Errore: codice di verifica non valido",Toast.LENGTH_LONG).show();
                            Log.e(TAG,"Codice di verifica non valido");
                        }

                        Log.i(TAG,"Utente creato con successo");

                        Intent homeActivity = new Intent(VerificationActivity.this, MainActivity.class);
                        startActivity(homeActivity);
                        finish();

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(TAG,"Errore sconosciuto durante la fase di verifica del codice");
                        Toast.makeText(VerificationActivity.this,"Errore sconosciuto",Toast.LENGTH_LONG).show();
                    }
                });

                //torna al login se registrazione avvenuta con successo
                Intent loginActivity = new Intent(VerificationActivity.this, LoginActivity.class);
                startActivity(loginActivity);
                finish();
            }
        });
    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent loginActivity = new Intent(VerificationActivity.this, RegistrationActivity.class);
        startActivity(loginActivity);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent loginActivity = new Intent(VerificationActivity.this, RegistrationActivity.class);
        startActivity(loginActivity);
        finish();
        super.onBackPressed();
    }
}
