package it.cvm.consigliaviaggimobile.activities.Login.ManagePassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.ForgotPasswordResult;
import com.amazonaws.mobile.client.results.SignUpResult;
import com.example.consigliaviaggimobile.R;

import androidx.appcompat.app.AppCompatActivity;
import it.cvm.consigliaviaggimobile.activities.Login.LoginActivity;

public class ResetPasswordActivity extends AppCompatActivity {

    private final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final EditText emailText = findViewById(R.id.emailText);
        final Button sendButton = findViewById(R.id.sendButton);
        final TextView resendVerificationCode = findViewById(R.id.resendVerificationCodeButton);


        //Password dimenticata
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Controlla che è stata scritta l'email per ricevere il codice
                if(emailText.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Inserire username per recuperare la password",Toast.LENGTH_LONG).show();
                }else {
                    Log.i(TAG,"Processo recupero password avviato");
                    AWSMobileClient.getInstance().forgotPassword(emailText.getText().toString(), new Callback<ForgotPasswordResult>() {
                        @Override
                        public void onResult(final ForgotPasswordResult result) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    switch (result.getState()){
                                        //Se il verification code viene mandato correttamente per email, manda nel forgotPassword Activity
                                        case CONFIRMATION_CODE: Log.i(TAG,"Codice di conferma inviato per email");
                                            Toast.makeText(getApplicationContext(),"Codice di conferma inviato per email",Toast.LENGTH_LONG).show();
                                            Intent forgotActivity = new Intent(ResetPasswordActivity.this, SecurityCodeActivity.class);
                                            forgotActivity.putExtra("username",emailText.getText().toString());
                                            startActivity(forgotActivity);
                                            finish();
                                            break;
                                        default: Log.e(TAG,"Errore codice di verifica per il reset password non inviato");
                                            Toast.makeText(getApplicationContext(),"Errore: non è possibiile ripristinare la password, contattare l'admin",Toast.LENGTH_LONG).show();
                                            break;
                                    }

                                }
                            });
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e(TAG,"Errore durante il processo di recupero password");
                        }
                    });
                }
            }
        });

        //Rimanda il codice di verifica inserendo l'username
        resendVerificationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailText.getText().toString().isEmpty()){
                    Log.e(TAG,"Non è stato inserito alcun username");
                    Toast.makeText(getApplicationContext(),"Inserire username per reinviare il codice",Toast.LENGTH_SHORT).show();
                }else{

                    AWSMobileClient.getInstance().resendSignUp(emailText.getText().toString(), new Callback<SignUpResult>() {
                        @Override
                        public void onResult(SignUpResult result) {
                            Log.i(TAG,"Verification Code resendend to "+ result.getUserCodeDeliveryDetails().getDestination());
                            Intent verificationCodeActivity = new Intent(ResetPasswordActivity.this, SecurityCodeActivity.class);
                            verificationCodeActivity.putExtra("username",emailText.getText().toString());

                            startActivity(verificationCodeActivity);
                            finish();
                        }

                        @Override
                        public void onError(Exception e) {
                            Toast.makeText(getApplicationContext(),"Errore durante il recupero dell'username",Toast.LENGTH_SHORT).show();
                            Log.e(TAG,"Errore durante la fase di Resend Verification Code");
                        }
                    });

                }
            }
        });

    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent loginActivity = new Intent(ResetPasswordActivity.this, LoginActivity.class);
        startActivity(loginActivity);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent loginActivity = new Intent(ResetPasswordActivity.this, LoginActivity.class);
        startActivity(loginActivity);
        finish();
        super.onBackPressed();
    }
}
