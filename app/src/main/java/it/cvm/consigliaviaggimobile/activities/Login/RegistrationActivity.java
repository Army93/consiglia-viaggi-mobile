package it.cvm.consigliaviaggimobile.activities.Login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignUpResult;
import com.example.consigliaviaggimobile.R;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class RegistrationActivity extends AppCompatActivity {

    private final String TAG = RegistrationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        final EditText usernameInput = findViewById(R.id.usernameInputText);
        final EditText nomeInput = findViewById(R.id.nameInputText);
        final EditText cognomeInput = findViewById(R.id.cognomeInputText);
        final EditText emailInput = findViewById(R.id.emaiInputText);
        final EditText pswInput = findViewById(R.id.passwordInputText);
        final EditText confirmPsw = findViewById(R.id.confirmPswInputText);

        Button submit = findViewById(R.id.submitRegistrationButton);

        //Mostra pulsante indietro action bar
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Invia la richiesta di login ad AWS
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = usernameInput.getText().toString();
                final String name = nomeInput.getText().toString();
                final String surname = cognomeInput.getText().toString();
                final String email = emailInput.getText().toString();
                final String psw = pswInput.getText().toString();
                final String confPsw = confirmPsw.getText().toString();

                final Map<String, String> attributes = new HashMap<>();

                attributes.put("email", emailInput.getText().toString());
                attributes.put("name", nomeInput.getText().toString());
                attributes.put("family_name", cognomeInput.getText().toString());
                attributes.put("nickname", usernameInput.getText().toString());

                //Verifica che tutti i campi siano stati inseriti
                if (username.isEmpty() || name.isEmpty() || surname.isEmpty() || email.isEmpty() || psw.isEmpty() || confPsw.isEmpty() ) {

                    Toast.makeText(getApplicationContext(), "Inserire tutti i campi richiesti", Toast.LENGTH_LONG).show();
                } else {
                    if(psw.equals(confPsw)){
                        //Creo la richiesta di registrazione con Cognito
                        AWSMobileClient.getInstance().signUp(username, psw, attributes, null, new Callback<SignUpResult>() {

                            @Override
                            public void onResult(SignUpResult result) {

                                //Se si solleva un'errore durante la fase di registazione
                                if (!result.getConfirmationState()) {
                                    Log.e(TAG, "Registrazione non avvenuta con successo");
                                }

                                //Passa alla fase di verifica email tramite codice
                                Log.i(TAG, "Registrazione passata al passo successivo, aspetto il verification code");

                                Intent verificationActivity = new Intent(RegistrationActivity.this, VerificationActivity.class);
                                verificationActivity.putExtra("username", username);
                                startActivity(verificationActivity);
                                finish();
                            }

                            @Override
                            public void onError(Exception e) {
                                Log.e(TAG, "Errore durante la richiesta di registrazione: " + e.getMessage());
                            }
                        });
                    }else{
                        Toast.makeText(getApplicationContext(), "Password non coincidono", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    //Pulsante indietro action bar
    @Override
    public boolean onSupportNavigateUp() {
        Intent loginPage = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivity(loginPage);
        finish();
        return super.onSupportNavigateUp();
    }

    //Pulsante indietro navigation bar
    @Override
    public void onBackPressed() {
        Intent loginPage = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivity(loginPage);
        finish();
        super.onBackPressed();
    }
}
