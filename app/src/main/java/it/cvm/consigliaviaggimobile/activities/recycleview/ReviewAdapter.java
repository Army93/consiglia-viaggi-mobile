package it.cvm.consigliaviaggimobile.activities.recycleview;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;
import com.example.consigliaviaggimobile.R;

import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder>{
    private final String TAG = ReviewAdapter.class.getSimpleName();

    private List<Document> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ConstraintLayout textView;

        public MyViewHolder(ConstraintLayout v) {
            super(v);
            textView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReviewAdapter(List<Document> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_review_layout, parent, false);

        ReviewAdapter.MyViewHolder vh = new ReviewAdapter.MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ReviewAdapter.MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        TextView nomeRecensione = (TextView) holder.textView.getViewById(R.id.titleReviewText);
        EditText commentoRecensione = (EditText) holder.textView.getViewById(R.id.reviewText);
        TextView dataRecensione = (TextView) holder.textView.getViewById(R.id.dataReviewFrameText);
        TextView nomeUtente = (TextView) holder.textView.getViewById(R.id.nameReviewerText);
        RatingBar ratingBar = (RatingBar) holder.textView.getViewById(R.id.ratingReview);
        TextView dataUltimaVisita = (TextView) holder.textView.getViewById(R.id.dateVisitText);

        //Popola il layout con i dati presi da DynamoDB
        ratingBar.setRating(mDataset.get(position).get("Valutazione").asInt());

        nomeUtente.setText(mDataset.get(position).get("NomeUtente").asString());
        nomeRecensione.setText(mDataset.get(position).get("Titolo").asString());
        commentoRecensione.setText(mDataset.get(position).get("Commento").asString());
        dataRecensione.setText(mDataset.get(position).get("DataRecensione").asString());
        dataUltimaVisita.setText(mDataset.get(position).get("DataUltimaVisita").asString());

        Log.i(TAG,"Popolate tutte le recensioni della struttura");

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
