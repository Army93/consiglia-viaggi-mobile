package it.cvm.consigliaviaggimobile.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;

import java.util.Map;

public class UserTask extends AsyncTask<Void, Void, String> {
    private static String TAG = UserTask.class.getSimpleName();

    String fullName = null;

    @Override
    protected String doInBackground(Void... voids) {
        try {
            //Get Attributes Map
            Map<String, String> userAttributes = AWSMobileClient.getInstance().getUserAttributes();
            //Build fullname
            fullName = userAttributes.get("name")+" "+userAttributes.get("family_name");
        } catch (Exception e) {
            Log.e(TAG,"Errore durante l'estrazione degli attributi dell'utente");
        }
        return fullName;
    }
}