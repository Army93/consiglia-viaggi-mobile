package it.cvm.consigliaviaggimobile.utils;

import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReviewsUtils {

    public final static String TAG = ReviewsUtils.class.getSimpleName();
    /**
     * Controlla se l'utente ha già inserito una recensione per quella struttura
     * @param listaRecensioni Lista delle recensioni di una struttura
     * @param utente utente da cercare
     * @return boolean true se utente giù inserito recensione, false altrimenti
     */
    public static boolean checkIfReviewExists(List<Document> listaRecensioni, String utente){

        for(Document recensione : listaRecensioni){
            if(utente.equalsIgnoreCase(recensione.get("NomeUtente").asString())){
                return true;
            }
        }

        return false;
    }

    /**
     * Crea una lista filtrata in base al mese, passato tramite enum Month
     * @param listarecensioni lista delle recensioni
     * @param month numero del mese in base alla posizione nello spinner
     * @return Lista fitrata di recensioni per mese
     */
    public static List<Document> filtraReviewByMonth(List<Document> listarecensioni, int month){
       List<Document> listaFiltrata = new ArrayList<>();

       if(month == 0){
           return listarecensioni;
       }

       MonthUtils monthUtils = new MonthUtils();

        for(Document doc : listarecensioni){

            //Get Date from db list
            String dataRecensione = Objects.requireNonNull(doc.get("DataUltimaVisita")).asString();
            String splitDate[] = dataRecensione.split("\\s+");
            String currentMonthList = splitDate[0];

            String selectedMonth = monthUtils.getMonthByNumber(month);

                    if(currentMonthList.equalsIgnoreCase(selectedMonth)){
                        listaFiltrata.add(doc);
                    }
            }

        if(listaFiltrata.size() == 0){

            Log.w(TAG,"Lista filtrata ha size 0");
            return listaFiltrata;
        }

        Log.i(TAG,"Lista filtrata ha size: "+listaFiltrata.size());

        return listaFiltrata;
    }

    /**
     * Crea una lista filtrata in base al numero di stelle a esso assegnata
     * @param documentList lsita delle recensioni
     * @param stars numero di stelle pertinenti
     * @return lista filtrata in base al numero di stelle delle recensioni di una struttura
     */
    public static List<Document> filtraReviewByRating(List<Document> documentList, int stars){
        List<Document> listaFiltrata = new ArrayList<>();

        if(stars == 0){
            return documentList;
        }

        for(Document doc : documentList){

            int stelleRecensione = Objects.requireNonNull(doc.get("Valutazione")).asInt();

            if(stelleRecensione == stars){
                listaFiltrata.add(doc);
            }
        }

        if(listaFiltrata.size() == 0){

            Log.w(TAG,"Lista filtrata in base al numero di stelle assegnate ha size 0");
            return listaFiltrata;
        }

        Log.i(TAG,"Lista filtrata in base a "+stars+" stelle ha size: "+listaFiltrata.size());

        return listaFiltrata;
    }
}
