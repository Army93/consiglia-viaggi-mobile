package it.cvm.consigliaviaggimobile.utils;

import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;

import java.util.ArrayList;
import java.util.List;

import it.cvm.consigliaviaggimobile.models.SearchBeanStructures;

public class StructuresUtils {

    private final static String TAG = StructuresUtils.class.getSimpleName();

    List<Document> listStructures;

    public StructuresUtils(List<Document> listStructures){
        this.listStructures = listStructures;
    }


    /**
     * Genera una lista delle cinque strutture passate da una lista di strutture
     * @param structures
     * @return List di strutture
     */
    public List<Document> getBestFiveStructures(List<Document> structures){
        List<Document> fiveBest = new ArrayList<>();

        //Preleva solo le strutture da 5 stelle a salire
            for (Document structure : structures) {
                if(structure.get("Stelle").asInt() > 3){
                    fiveBest.add(structure);
                }
            }

       return fiveBest;
    }

    /**
     * Filtra la lista delle strutture suggerite in fase di ricerca
     * in base alla valutazione indicata tramite spinner
     * @param fullList Lista con strutture
     * @param position numero di stelle della struttura
     * @return Lista filtrata
     */
    public static List<SearchBeanStructures> structuresFilteredByStars(List<SearchBeanStructures> fullList, int position){
        List<SearchBeanStructures> filterList = new ArrayList<>();

        if(position == 0){
            return fullList;
        }

        Log.i(TAG,"Costruisco la lista di strutture filtrate in base a "+position+" stelle");
        for(SearchBeanStructures bean : fullList){
            if(bean.getValutazione() == position){
                filterList.add(bean);
            }
        }

        if(filterList.size() == 0){
            Log.w(TAG,"Lista vuota di struttute con "+position+" stelle");
        }

        return filterList;
    }

    /**
     * Filtra la lista delle strutture in base ad un range di prezzo
     * @param fullList Lista completa di strutture
     * @param position 0 = mostra tutto, 1 strutture economiche, 2 nella media, 3 raffinate
     * @return Lista di strutture con prezzo compreso tra min e max (compresi gli uguali)
     */
    public static List<SearchBeanStructures> structuresFilteredByPrice(List<SearchBeanStructures> fullList, int position){
        List<SearchBeanStructures> filterList = new ArrayList<>();
        int minPrice=0;
        int maxPrice=0;

        //tutte le strutture
        if(position == 0){ return fullList; }

        Log.i(TAG,"Costruisco la lista di strutture filtrate in base alla fascia economica selezionata");

        //Set Prices by Position
        switch (position){
            case 1: minPrice = 0;
                    maxPrice = 20;
                    break;
            case 2: minPrice = 21;
                    maxPrice = 50;
                    break;
            case 3: minPrice = 51;
                    maxPrice = 10000000;
                    break;
            default: Log.e(TAG,"Immesso un valore non consentito");
                   return fullList;
        }

        //filter by price
        for(SearchBeanStructures bean : fullList){
            if(bean.getPrezzo() >= minPrice && bean.getPrezzo() <= maxPrice){
                filterList.add(bean);
            }
        }

        if(filterList.size() == 0){
            Log.w(TAG,"Lista vuota di struttute");
        }

        return filterList;
    }

    /**
     * Calcola la fascia di prezzo in base al prezzo recuperato dalla struttura
     * @param prezzo prezzo recuperato dalla struttura
     * @return Stringa riguardante fascia di prezzo
     */
    public static String getFasciaByPrice(float prezzo){

       Log.i(TAG,"Calcolo la fascia di prezzo in base al prezzo passato");

       if(prezzo >= 0 && prezzo <= 20){
           return "Economico";
       }else if(prezzo >=21 && prezzo <= 50){
           return "Nella Media";
       }else {
           return "Raffinato";
       }
    }


}
