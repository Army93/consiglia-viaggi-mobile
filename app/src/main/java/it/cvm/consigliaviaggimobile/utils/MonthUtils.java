package it.cvm.consigliaviaggimobile.utils;

import java.util.HashMap;
import java.util.Map;

public class MonthUtils {

    private Map<Integer , String> monthMap = new HashMap<>();

    public MonthUtils(){
        monthMap.put(1,"gennaio");
        monthMap.put(2,"febbraio");
        monthMap.put(3,"marzo");
        monthMap.put(4,"aprile");
        monthMap.put(5,"maggio");
        monthMap.put(6,"giugno");
        monthMap.put(7,"luglio");
        monthMap.put(8,"agosto");
        monthMap.put(9,"settembre");
        monthMap.put(10,"ottobre");
        monthMap.put(11,"novembre");
        monthMap.put(12,"dicembre");
    }

    /**
     * Get Month name from position on spinner array
     * @param position
     * @return name of the corresponding month
     */
    public String getMonthByNumber(int position){
        return monthMap.get(position);
    }

}
