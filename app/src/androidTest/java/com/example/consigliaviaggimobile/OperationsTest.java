package com.example.consigliaviaggimobile;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.dynamodbv2.document.datatype.Document;
import com.google.android.gms.common.internal.Asserts;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionReviews;
import it.cvm.consigliaviaggimobile.dynamodb.controllers.ConnectionStructures;
import it.cvm.consigliaviaggimobile.models.Recensione;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class OperationsTest {

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.consigliaviaggimobile", appContext.getPackageName());
    }

    @Test
    public void testLoginConnection(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        AWSMobileClient.getInstance().initialize(appContext, new Callback<UserStateDetails>() {
            @Override
            public void onResult(UserStateDetails result) {
            assertEquals("LOG_IN",result.getUserState().toString());
            }

            @Override
            public void onError(Exception e) {
            }
        });
        }

    @Test
    public void getStructuresNumber(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ConnectionStructures dbConnection = new ConnectionStructures(appContext,"Strutture");
        List<Document> strutture = dbConnection.getALLStrutture();
        assertEquals(13,strutture.size());
    }

    @Test
    public void getAllReviews(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ConnectionReviews dbConnection = new ConnectionReviews(appContext,"Recensioni");
        List<Document> recensioni = dbConnection.getAllReviews();
        assertEquals(23,recensioni.size());
    }

    @Test
    public void getStrutturaByCoordinate(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ConnectionStructures dbConnection = new ConnectionStructures(appContext,"Strutture");
        Document struttura = dbConnection.getStrutturaByCoordinates("40.8253411","14.1913169");
        Asserts.checkNotNull(struttura);
    }

    @Test
    public void getNameStrutturaByCoordinate(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ConnectionStructures dbConnection = new ConnectionStructures(appContext,"Strutture");
        Document struttura = dbConnection.getStrutturaByCoordinates("40.8253411","14.1913169");
        assertEquals("Hotel Esedra", Objects.requireNonNull(struttura.get("Nome")).asString());
    }

    @Test
    public void getReviewsFromStructure(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ConnectionReviews dbConnection = new ConnectionReviews(appContext,"Recensioni");
        List<Document> recensioni = dbConnection.getAllReviewsFromStructure("Hotel Esedra");
        assertEquals(2,recensioni.size());
    }

    @Test
    public void addReview(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ConnectionReviews dbConnection = new ConnectionReviews(appContext,"Recensioni");
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH)+1;
        String timestamp = calendar.get(Calendar.DAY_OF_MONTH)+"/"+month+"/"+calendar.get(Calendar.YEAR);
        int stelle = new Random().nextInt(5)+1;
        String nomeStruttura="Hotel Esedra";
        String titoloRecensione="Stupendo";
        String dataUltimaVisita="aprile 2018"; //respect format -> maggio 2020, aprile 2012 ...
        String proprietario="AndroidTest";
        String commento="Questo è un test effettuato tramite UnitTest Android per l'inserimento di una recensione per la struttura "+nomeStruttura;
        Recensione recensione = new Recensione(nomeStruttura,
                                    titoloRecensione,
                                    timestamp,
                                    dataUltimaVisita,
                                    proprietario,
                                    stelle,
                                    commento);

        //dbConnection.addReview(recensione); //da usare con cautela
    }

    @Test
    public void getReviewOwner(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        List<Document> ownerReview = new ArrayList<>();
        String proprietarioRecensione="army93";
        String strutturaRecensita ="Hotel Esedra";

        ConnectionReviews dbConnection = new ConnectionReviews(appContext,"Recensioni");
        List<Document> recensioni = dbConnection.getAllReviewsFromStructure(strutturaRecensita);

        for(Document document : recensioni){
            if(Objects.requireNonNull(document.get("NomeUtente")).asString().equals(proprietarioRecensione)){
                ownerReview.add(document);
            }
        }

        if(ownerReview.size() > 0){
            assertEquals(proprietarioRecensione, Objects.requireNonNull(ownerReview.get(0).get("NomeUtente")).asString());
        }else {
            Log.e("getReviewOwner","Recensione per utente "+proprietarioRecensione+" non trovata per la struttura "+strutturaRecensita);
        }
    }

}
